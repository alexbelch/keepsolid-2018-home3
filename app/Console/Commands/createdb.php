<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use PDO;
use PDOException;

class createdb extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    //protected $signature = 'command:name';
    protected $signature = 'createdb {namedb? : The name of the new DB}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new DB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        try{
            $namedb = $this->argument('namedb');
            //$this->info("namedb=" . $namedb);
            if (!$namedb) {
                $namedb = env('DB_DATABASE', 'forge') . 1;
                //$this->info("namedb=" . $namedb);
            }
            if ($namedb) {
                $result = DB::statement(DB::raw('CREATE DATABASE ' . $namedb));
                $this->info("Database ". $namedb . " created!!! Result=" . $result);
                $result = DB::statement(DB::raw("CREATE USER " . $namedb . " WITH ENCRYPTED PASSWORD '123456'"));
                $this->info("User ". $namedb . " created!!! Result=" . $result);
                $result = DB::statement(DB::raw('GRANT ALL PRIVILEGES ON DATABASE ' . $namedb . ' TO ' . $namedb));
                $this->info("Grant all to user ". $namedb . " granted!!! Result=" . $result);
            }
            else {
                $result = DB::statement(DB::raw('CREATE DATABASE forge'));

                $this->info("Database forge created!!! Result=" . $result);
            }

            if (($result == 1) && ($this->confirm('Do you wish set new value to .env file in ?'))) {
                $envKeyDB = 'DB_DATABASE';
                $envKeyUser = 'DB_USERNAME';
                $envKeyPas = 'DB_PASSWORD';
                $envValue = $namedb;

                $envFile = app()->environmentFilePath();

                $str = file_get_contents($envFile);
                //$this->info("file env=" . $str);

                $oldValue = env($envKeyDB); //strtok($str, "{$envKey}=");
                //$this->info("oldValue env=" . $oldValue);
                $str = str_replace("{$envKeyDB}={$oldValue}", "{$envKeyDB}={$namedb}",$str);
                //$this->info("edit file env=" . $str);

                $oldValue = env($envKeyUser);
                //$this->info("oldValue env=" . $oldValue);
                $str = str_replace("{$envKeyUser}={$oldValue}", "{$envKeyUser}={$namedb}",$str);
                //$this->info("edit file env=" . $str);

                $oldValue = env($envKeyPas);
                ///$this->info("oldValue env=" . $oldValue);
                $str = str_replace("{$envKeyPas}={$oldValue}", "{$envKeyPas}=123456",$str);
                //$this->info("edit file env=" . $str);

                $fp = fopen($envFile, 'w');
                fwrite($fp, $str);
                fclose($fp);
                $this->info("New value to .env file saved!");
            }
        }
        catch (\Exception $e){
            $this->info("Error create database !!! Result=" . $e->getMessage());
            die("Could not connect to the postgress DB = " . env('DB_DATABASE', 'forge') . ". \nPlease check your configuration or set DB_DATABASE=postgres \n");
        }
    }
}
