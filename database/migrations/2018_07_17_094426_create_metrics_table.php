<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metrics', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ip_address');
            $table->string('referer');
            $table->string('user_agent');
            $table->integer('advert_id')->unsigned();
            $table->timestamps();
            $table->foreign('advert_id')
                ->references('id')->on('adverts')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('metrics', function (Blueprint $table) {
            $table->dropForeign('metrics_advert_id_foreign');
        });
        Schema::dropIfExists('metrics');
    }
}
